#[allow(unused_imports)]
use crate::{impl_len, impl_len_kv, Len, Length, SmallLength};

#[cfg(feature = "indexmap")]
impl_len_kv!(indexmap::IndexMap<K, V>);


#[cfg(feature = "bytes")]
impl_len!(bytes::Bytes bytes::BytesMut);

#[cfg(feature = "bumpalo")]
impl Len for bumpalo::Bump {
    #[inline]
    fn length(&self) -> Length {
        self.allocated_bytes().into()
    }
}

#[cfg(feature = "var_num")]
impl From<SmallLength> for var_num::VarUInt {
    #[inline]
    fn from(value: SmallLength) -> Self {
        match value {
            SmallLength::Byte(b) => var_num::VarUInt::U8(b),
            SmallLength::Word(b) => var_num::VarUInt::U16(b),
            SmallLength::Double(b) => var_num::VarUInt::U32(b),
            SmallLength::Quad(b) => var_num::VarUInt::U64(b),
        }
    }
}

#[cfg(feature = "var_num")]
impl From<SmallLength> for var_num::VarNum {
    #[inline]
    fn from(value: SmallLength) -> Self {
        var_num::VarNum::UInt(value.into())
    }
}

#[cfg(feature = "var_num")]
impl From<Length> for var_num::VarNum {
    #[inline]
    fn from(value: Length) -> Self {
        value.index().into()
    }
}

#[cfg(feature = "var_num")]
impl From<Length> for var_num::VarUInt {
    #[inline]
    fn from(value: Length) -> Self {
        value.index().into()
    }
}