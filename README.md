# small_len

A simple enum and trait to ensure that the value returned by small_len() is always the smallest representation. This is 
meant to be used by [fn_vm](https://docs.rs/fn_vm/latest/fn_vm/) to support "infinite" commands, arguments, and registers.

NOTE: This is primarily meant for dynamic sized objects, String, [T], &[T], Vec, HashMap, and IndexMap (with feature 
indexmap); if you need an optimization for static bounds at compile time check out the 
[smallnum](https://docs.rs/smallnum/latest/smallnum/) crate.

For a generic number crate check out [varnum](https://docs.rs/varnum/latest/varnum/).

## Usage

```rust
use small_len::SmallLen;

fn main() {
    let a = vec![1, 2, 3];
    let c = a.small_len(); // Length::Byte(3)
    let bytes = c.to_be_bytes();
    let c = SmallLen::from_be_bytes(&bytes); // SmallLen::from_bytes() -> Length::Byte(3)
}
```

## Features

- `default`: No extra features, includes Vec<T> and HashMap<K, V> implementations.
- `bumpalo`: Adds `Len` implementation for `bumpalo::Bump`.
- `bytes`: Adds `Len` implementation for `bytes::Bytes` and `bytes::BytesMut`.
- `indexmap`: Adds `Len` implementation for `indexmap::IndexMap`.

## Custom Types

If you need to add SmallLen to another type, you can implement the Len trait.

```rust
impl <T> Len for Vec<T> {
    #[inline]
    fn length(&self) -> Length {
        self.len().into() // Length::new(self.len())
    }
}
```

## Additional Traits
The Length enum also implements the following traits for easier use (Length | SmallLength | usize):
- Index for Vec<T>, this will panic if the index is out of bounds
- Add
- Div
- Mul
- Rem
- Sub
- Not
- BitAnd
- BitOr
- BitXor
